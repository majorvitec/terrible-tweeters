# Terrible Tweeters [2021]

Created a Angry Bird like game [Followed Tutorial].

## Source

[How to Make a Game - Unity Beginner Tutorial - 2021 Version!](https://www.youtube.com/watch?v=Lu76c85LhGY)

## Game Overview

### Level 1 Screen

![Level 1 Screen](/images/readme/play_screen_level1.png "Level 1 Screen")

### Level 2 Screen

![Level 2 Screen](/images/readme/play_screen_level2.png "Level 2 Screen")
